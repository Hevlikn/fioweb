using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Timers;
using System.Threading.Tasks;

using FIOWeb.Components;
using FIOWeb.JsonPayloads;
using FIOWeb.UniverseMap;

using Newtonsoft.Json;

using MatBlazor;

namespace FIOWeb.Pages
{
    public partial class GroupHub2
    {
        private MapComponent MapComponent { get; set; }
        private GroupHubModel GroupHubModel = null;

        private List<string> UsersToRetrieve = null;

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            if (NavManager.TryGetQueryString("Users", out string Users))
            {
                var UsersSplitRes = Users.Split(new String[] { "__" }, StringSplitOptions.RemoveEmptyEntries);
                UsersToRetrieve = UsersSplitRes.ToList();
            }
            else if (NavManager.TryGetQueryString("Group", out string Group))
            {
                var GroupSplitRes = Group.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                if (GroupSplitRes.Length > 0)
                {
                    int GroupId;
                    if (int.TryParse(GroupSplitRes[0], out GroupId))
                    {
                        var groupRequest = new Web.Request(HttpMethod.Get, $"/auth/group/{GroupId}", await GlobalAppState.GetAuthToken());
                        var group = await groupRequest.GetResponseAsync<GroupPayload>();
                        UsersToRetrieve = group.GroupUsers.Select(gu => gu.GroupUserName).ToList();
                    }
                }
            }
        }

        private Dictionary<string, string> NaturalIdToSystemGuid;

        int ProgressCount = 0;
        private void SetProgress(int NewProgress)
        {
            ProgressCount = NewProgress;
            StateHasChanged();
        }

        private bool IsLoaded = false;
        private async Task Refresh()
        {
            SetProgress(0);

            MapComponent.SceneDescription.DrawPlayerBaseSprites = true;
            MapComponent.SceneDescription.DrawPlayerStationaryShipSprites = true;
            MapComponent.SceneDescription.DrawInFlightShips = true;

            var groupHubRequest = new Web.Request(HttpMethod.Post, "/fioweb/grouphub", await GlobalAppState.GetAuthToken(), JsonConvert.SerializeObject(UsersToRetrieve));
            GroupHubModel = await groupHubRequest.GetResponseAsync<GroupHubModel>();

            NaturalIdToSystemGuid = MapComponent.SceneDescription.Systems.ToDictionary(kvp => kvp.Value.NaturalId, kvp => kvp.Key);

            PopulatePlayerBases();
            PopulateStationaryShips();
            PopulateInFlightShips();
            SetProgress(1);
            IsLoaded = true;
        }

        private void PopulatePlayerBases()
        {
            foreach (var PlayerModel in GroupHubModel.PlayerModels)
            {
                var UserName = PlayerModel.UserName;
                
                foreach (var Location in PlayerModel.Locations)
                {
                    var SystemNaturalId = Location.LocationIdentifier.TrimEnd(Location.LocationIdentifier[Location.LocationIdentifier.Length - 1]);
                    MapComponent.SceneDescription.Systems[NaturalIdToSystemGuid[SystemNaturalId]].HasPlayerBase |= (Location.BaseStorage != null);
                    MapComponent.SceneDescription.Systems[NaturalIdToSystemGuid[SystemNaturalId]].HasPlayerStorage = true;

                    var NewPlayerBase = new PlayerBase();
                    NewPlayerBase.UserName = UserName;
                    NewPlayerBase.LocationIdentifier = Location.LocationIdentifier;
                    NewPlayerBase.LocationName = Location.LocationName;
                    MapComponent.SceneDescription.Systems[NaturalIdToSystemGuid[SystemNaturalId]].PlayerBases.Add(NewPlayerBase);

                    if (Location.BaseStorage != null)
                    {
                        var storage = new UniverseMap.PlayerStorage();
                        storage.AddressableId = Location.LocationIdentifier;
                        storage.UserName = UserName;
                        storage.InventoryType = Location.BaseStorage.StorageType;
                        storage.LocationIdentifier = Location.LocationIdentifier;
                        storage.LocationName = Location.LocationName;
                        MapComponent.SceneDescription.Systems[NaturalIdToSystemGuid[SystemNaturalId]].PlayerStorages.Add(storage);
                    }

                    if (Location.WarehouseStorage != null)
                    {
                        var storage = new UniverseMap.PlayerStorage();
                        storage.AddressableId = Location.LocationIdentifier;
                        storage.UserName = UserName;
                        storage.InventoryType = Location.WarehouseStorage.StorageType;
                        storage.LocationIdentifier = Location.LocationIdentifier;
                        storage.LocationName = Location.LocationName;
                        MapComponent.SceneDescription.Systems[NaturalIdToSystemGuid[SystemNaturalId]].PlayerStorages.Add(storage);
                    }
                }
            }
        }

        private void PopulateStationaryShips()
        {
            var NaturalIdToPlayerShips = new Dictionary<string, List<PlayerShip>>();
            foreach (var stationaryShip in GroupHubModel.PlayerStationaryShips)
            {
                var naturalId = stationaryShip.AddressLines
                    .Select(al => al.NaturalId)
                    .FirstOrDefault();
                if (naturalId != null)
                {
                    if (!NaturalIdToPlayerShips.ContainsKey(naturalId))
                    {
                        NaturalIdToPlayerShips.Add(naturalId, new List<PlayerShip>());
                    }

                    NaturalIdToPlayerShips[naturalId].Add(stationaryShip);
                }
            }

            var SystemGuidToPlayerShips = MapComponent.SceneDescription.Systems
                .Where(kvp => NaturalIdToPlayerShips.Keys.Contains(kvp.Value.NaturalId))
                .ToDictionary(kvp => kvp.Key, kvp => NaturalIdToPlayerShips[kvp.Value.NaturalId]);

            foreach (var kvp in SystemGuidToPlayerShips)
            {
                var SystemGuid = kvp.Key;
                var PlayerShips = kvp.Value;

                MapComponent.SceneDescription.Systems[SystemGuid].HasSystemShips = true;
                var PlayerShipsToAdd = PlayerShips
                    .Select(ps => new PlayerSystemShip
                    {
                        UserName = ps.PlayerName,
                        ShipName = ps.ShipName,
                        ShipRegistration = ps.ShipRegistration
                    })
                    .ToList();
            }
        }

        public class FlightData
        {
            public string ShipObjectGuid { get; set; }
            public string ActiveFTLObjectGuid { get; set; }
            public double ShipLocationX { get; set; }
            public double ShipLocationY { get; set; }
            public double ShipLocationZ { get; set; }
            public double ShipRotationAngle { get; set; }
            public List<string> FTLObjectGuidsThatWePassed { get; set; } = new List<string>();
            public List<string> FTLObjectGuidsThatWeNeedToPass { get; set; } = new List<string>();
        }

        public List<FlightData> AllFlightData = new List<FlightData>();

        private void PopulateInFlightShips()
        {
            var Now = Utils.GetCurrentEpochMs();
            var StartEndToGuid = MapComponent.SceneDescription.FTLConnections.ToDictionary(kvp => $"{kvp.Value.StartSystemId}-{kvp.Value.EndSystemId}", kvp => kvp.Key);
            var SystemNaturalIdToSystemGuids = MapComponent.SceneDescription.SystemNaturalIdToSystemGuids;
            foreach (var inFlightShip in GroupHubModel.PlayerShipsInFlight)
            {
                var ShipFlightData = new FlightData();
                ShipFlightData.ShipObjectGuid = Guid.NewGuid().ToString();
                foreach (var segment in inFlightShip.Flight.Segments)
                {
                    if (segment.Type == "JUMP")
                    {
                        System.Diagnostics.Debug.Assert(segment.OriginLines.Count == 1);
                        System.Diagnostics.Debug.Assert(segment.DestinationLines.Count == 1);
                        var OriginNaturalId = segment.OriginLines[0].LineNaturalId;
                        var DestinationNaturalId = segment.DestinationLines[0].LineNaturalId;

                        var Key1 = $"{OriginNaturalId}-{DestinationNaturalId}";
                        var Key2 = $"{DestinationNaturalId}-{OriginNaturalId}";
                        bool bIsLeftToRight = StartEndToGuid.ContainsKey(Key1);
                        var FTLConnectionGuid = bIsLeftToRight ? StartEndToGuid[Key1] : StartEndToGuid[Key2];
                        var FTLConnectionObj = MapComponent.SceneDescription.FTLConnections[FTLConnectionGuid];

                        var DepartureTime = segment.DepartureTimeEpochMs;
                        var ArrivalTime = segment.ArrivalTimeEpochMs;
                        if (Now < DepartureTime)
                        {
                            ShipFlightData.FTLObjectGuidsThatWeNeedToPass.Add(FTLConnectionGuid);
                        }
                        else if (Now > ArrivalTime)
                        {
                            ShipFlightData.FTLObjectGuidsThatWePassed.Add(FTLConnectionGuid);
                        }
                        else
                        {
                            ShipFlightData.ActiveFTLObjectGuid = FTLConnectionGuid;

                            // Interpolate from Start -> End
                            var PercentageProgress = (double)(Now - DepartureTime) / (double)(ArrivalTime - DepartureTime);
                            var StartVertices = bIsLeftToRight ? FTLConnectionObj.StartVertices : FTLConnectionObj.EndVertices;
                            var EndVertices = bIsLeftToRight ? FTLConnectionObj.EndVertices : FTLConnectionObj.StartVertices;

                            ShipFlightData.ShipLocationX = Utils.Lerp(StartVertices[0], EndVertices[0], PercentageProgress);
                            ShipFlightData.ShipLocationY = Utils.Lerp(StartVertices[1], EndVertices[1], PercentageProgress);
                            ShipFlightData.ShipLocationZ = Utils.Lerp(StartVertices[2], EndVertices[2], PercentageProgress);
                        }
                    }
                }

                if (ShipFlightData.ActiveFTLObjectGuid != null)
                {
                    // We're in-flight in between planets
                    var InFlightShip = new ShipObject();
                    InFlightShip.UserName = inFlightShip.PlayerName;
                    InFlightShip.ShipName = inFlightShip.ShipName;
                    InFlightShip.ShipRegistration = inFlightShip.ShipRegistration;
                    InFlightShip.Vertices = new List<double>(3);
                    InFlightShip.Vertices.Add(ShipFlightData.ShipLocationX);
                    InFlightShip.Vertices.Add(ShipFlightData.ShipLocationY);
                    InFlightShip.Vertices.Add(ShipFlightData.ShipLocationZ);
                    InFlightShip.Color = 0xB88218;
                    InFlightShip.ColorOnHover = 0xF4A401;
                    InFlightShip.DotNetMethodToInvokeOnHover = "OnHover";
                    InFlightShip.DotNetMethodToInvokeOnClick = "OnClick";
                    MapComponent.SceneDescription.InFlightShips.Add(ShipFlightData.ShipObjectGuid, InFlightShip);
                }
                else
                {
                    // We're in-system
                    // Find the leg we're on
                    var ActiveSegment = inFlightShip.Flight.Segments.FirstOrDefault(s => Now >= s.DepartureTimeEpochMs && Now <= s.ArrivalTimeEpochMs);
                    if (ActiveSegment == null)
                    {
                        // We're at the last segment
                        ActiveSegment = inFlightShip.Flight.Segments.Last();
                    }

                    // Find the system destinationLine
                    var SystemAddressLine = ActiveSegment.DestinationLines.FirstOrDefault(dl => dl.Type == "system");
                    Debug.Assert(SystemAddressLine != null);
                    var System = MapComponent.SceneDescription.Systems[SystemNaturalIdToSystemGuids[SystemAddressLine.LineNaturalId]];
                    var Ship = new PlayerSystemShip();
                    Ship.UserName = inFlightShip.PlayerName;
                    Ship.ShipName = inFlightShip.ShipName;
                    Ship.ShipRegistration = inFlightShip.ShipRegistration;
                    System.PlayerSystemShips.Add(Ship);
                    System.HasSystemShips = true;
                }

                AllFlightData.Add(ShipFlightData);
            }
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }

        private async Task OnFinishedLoading()
        {
            await Refresh();
            MapComponent.MarkSceneDescriptionReady();
        }
    }
}