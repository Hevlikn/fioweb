﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;

using FIOWeb.JsonPayloads;

namespace FIOWeb.UniverseMap
{
    public class SystemMaterial
    {
        [JsonPropertyName("materialTicker")]
        public string MaterialTicker { get; set; }
        [JsonPropertyName("concentration")]
        public double Concentration { get; set; }
        [JsonPropertyName("dailyExtraction")]
        public double DailyExtraction { get; set; }
    }

    public class Planet
    {
        [JsonPropertyName("naturalId")]
        public string NaturalId { get; set; }
        [JsonPropertyName("name")]
        public string Name { get; set; }
        [JsonPropertyName("displayName")]
        public string DisplayName { get; set; }
        [JsonPropertyName("isFertile")]
        public bool IsFertile { get; set; }
        [JsonPropertyName("fertility")]
        public float Fertility { get; set; }
        [JsonPropertyName("surfaceType")]
        public string SurfaceType { get; set; }
        [JsonPropertyName("gravity")]
        public string Gravity { get; set; }
        [JsonPropertyName("pressure")]
        public string Pressure { get; set; }
        [JsonPropertyName("temperature")]
        public string Temperature { get; set; }

        [JsonPropertyName("resources")]
        public List<SystemMaterial> Resources { get; set; } = new List<SystemMaterial>();

        [JsonIgnore]
        public string BuildingMaterials
        {
            get
            {
                var ResultMaterials = new List<string>();
                switch(SurfaceType)
                {
                    case "Rocky":
                        ResultMaterials.Add("MCG");
                        break;
                    case "Gaseous":
                        ResultMaterials.Add("AEF");
                        break;
                }

                switch(Gravity)
                {
                    case "Low":
                        ResultMaterials.Add("MGC");
                        break;
                    case "High":
                        ResultMaterials.Add("BL");
                        break;
                }

                switch(Pressure)
                {
                    case "Low":
                        ResultMaterials.Add("SEA");
                        break;
                    case "High":
                        ResultMaterials.Add("HSE");
                        break;
                }

                switch(Temperature)
                {
                    case "Low":
                        ResultMaterials.Add("INS");
                        break;
                    case "High":
                        ResultMaterials.Add("TSH");
                        break;
                }

                return string.Join(", ", ResultMaterials);
            }
            set
            {
                // Intentionally empty
            }
        }

        [JsonIgnore]
        public string FertilityDisplay
        {
            get
            {
                if (!IsFertile)
                {
                    return "--";
                }
                else
                {
                    var Prefix = Fertility > 0.0f ? "+" : "";
                    return $"{Prefix}{Fertility:P2}";
                }
            }
            set
            {
                // Intentionally empty
            }
        }

        [JsonIgnore]
        public string ResourcesDisplay
        {
            get
            {
                var resourcesDispList = new List<string>();
                Resources.ForEach(material =>
                {
                    resourcesDispList.Add($"{material.MaterialTicker} @ {material.DailyExtraction:N2}/d");
                });
                return string.Join(", ", resourcesDispList);
            }
            set
            {
                // Intentionally empty
            }
        }
    }

    public class PlayerBase
    {
        [JsonPropertyName("userName")]
        public string UserName { get; set; }

        [JsonPropertyName("locationIdentifier")]
        public string LocationIdentifier { get; set; }

        [JsonPropertyName("locationName")]
        public string LocationName { get; set; }
    }

    public class PlayerStorage
    {
        [JsonPropertyName("addressableId")]
        public string AddressableId { get; set; }

        [JsonPropertyName("userName")]
        public string UserName { get; set; }

        [JsonPropertyName("inventoryType")]
        public string InventoryType { get; set; }

        [JsonPropertyName("locationIdentifier")]
        public string LocationIdentifier { get; set; }

        [JsonPropertyName("locationName")]
        public string LocationName { get; set; }
    }

    public class PlayerSystemShip
    {
        [JsonPropertyName("userName")]
        public string UserName { get; set; }

        [JsonPropertyName("shipName")]
        public string ShipName { get; set; }

        [JsonPropertyName("shipRegistration")]
        public string ShipRegistration { get; set; }
    }

    public class SystemObject : SceneObjectBase
    {
        private const uint COLOR_ON_HOVER = 0xFFBB00;
        private const uint COLOR_FACTION_CI = 0xE10909;
        private const uint COLOR_FACTION_NC = 0xF3C626;
        private const uint COLOR_FACTION_IC = 0x009920;
        private const uint COLOR_FACTION_AI = 0xFB5318;
        private const uint COLOR_FACTION_NEUTRAL = 0xCCCCCC;

        [JsonPropertyName("systemId")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string SystemId { get; set; }

        [JsonPropertyName("naturalId")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string NaturalId { get; set; }

        [JsonPropertyName("displayName")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string DisplayName { get; set; }

        [JsonPropertyName("systemFaction")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string SystemFaction { get; set; }

        [JsonPropertyName("starRadius")]
        public double StarRadius { get; set; }

        [JsonPropertyName("hasCX")]
        public bool HasCX { get; set; }

        [JsonPropertyName("cxNaturalId")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public string CXNaturalId;

        [JsonPropertyName("hasShipyard")]
        public bool HasShipyard { get; set; }

        [JsonPropertyName("shipyardLocationNaturalIds")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<string> ShipyardLocationNaturalIds { get; set; } = new List<string>();

        [JsonPropertyName("hasLocalMarket")]
        public bool HasLocalMarket { get; set; }

        [JsonPropertyName("localMarketLocationNaturalIds")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<string> LocalMarketLocationNaturalIds { get; set; } = new List<string>();

        [JsonPropertyName("hasPlayerBase")]
        public bool HasPlayerBase { get; set; }

        [JsonPropertyName("playerBases")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<PlayerBase> PlayerBases { get; set; } = new List<PlayerBase>();

        [JsonPropertyName("hasPlayerStorage")]
        public bool HasPlayerStorage { get; set; }

        [JsonPropertyName("playerStorages")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<PlayerStorage> PlayerStorages { get; set; } = new List<PlayerStorage>();

        [JsonPropertyName("hasSystemShips")]
        public bool HasSystemShips { get; set; }

        [JsonPropertyName("playerSystemShips")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<PlayerSystemShip> PlayerSystemShips { get; set; } = new List<PlayerSystemShip>();

        [JsonPropertyName("planets")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<Planet> Planets { get; set; } = new List<Planet>();

        [JsonPropertyName("systemMaterials")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingDefault)]
        public List<SystemMaterial> SystemMaterials { get; set; } = new List<SystemMaterial>();

        [JsonPropertyName("vertices")]
        public List<double> Vertices { get; set; } = new List<double>(3);

        [JsonPropertyName("iconVertices")]
        public List<List<double>> IconVertices { get; set; } = new List<List<double>>(8);

        public SystemObject GetStrippedCopy()
        {
            SystemObject copy = new SystemObject();
            copy.IsVisible = this.IsVisible;
            copy.Color = this.Color;
            copy.DotNetMethodToInvokeOnHover = this.DotNetMethodToInvokeOnHover;
            copy.ColorOnHover = this.ColorOnHover;
            copy.DotNetMethodToInvokeOnClick = this.DotNetMethodToInvokeOnClick;
            copy.ColorOnClick = this.ColorOnClick;
            copy.StarRadius = this.StarRadius;
            copy.HasShipyard = this.HasShipyard;
            copy.HasLocalMarket = this.HasLocalMarket;
            copy.HasPlayerBase = this.HasPlayerBase;
            copy.HasPlayerStorage = this.HasPlayerStorage;
            copy.HasSystemShips = this.HasSystemShips;
            copy.Vertices = this.Vertices.Select(v => v).ToList();
            copy.IconVertices = this.IconVertices.Select(v => v.Select(vv => vv).ToList()).ToList();

            return copy;
        }

        // Based loosely on radius values from here: https://www.enchantedlearning.com/subjects/astronomy/stars/startypes.shtml
        private static readonly Dictionary<string, float> StarTypeToRadius = new Dictionary<string, float>
        {
            { "O", 8.00f },
            { "B", 7.00f },
            { "A", 6.50f },
            { "F", 5.50f },
            { "G", 5.00f },
            { "K", 4.50f },
            { "M", 4.00f }
        };

        public static Dictionary<string, SystemObject> GetSystems(List<StationPayload> Stations, List<SystemStarPayload> SystemStars, List<Models.PlanetSearchModel> PlanetSearchModels)
        {
            var Systems = new Dictionary<string, SystemObject>();
            foreach (var systemStar in SystemStars)
            {
                var station = Stations.Where(s => s.SystemNaturalId == systemStar.NaturalId).FirstOrDefault();
                var planets = PlanetSearchModels.Where(psm => psm.PlanetNaturalId.StartsWith(systemStar.NaturalId)).ToList();

                var key = Guid.NewGuid().ToString();
                var so = new SystemObject();
                so.SystemId = systemStar.SystemId;
                so.NaturalId = systemStar.NaturalId;
                so.DisplayName = systemStar.DisplayName;

                var p = planets.FirstOrDefault(p => !String.IsNullOrEmpty(p.FactionCode));
                if (p != null)
                {
                    so.SystemFaction = p.FactionCode;
                }

                so.StarRadius = StarTypeToRadius.ContainsKey(systemStar.Type) ? StarTypeToRadius[systemStar.Type] : 1.0f;
                so.StarRadius *= 0.66;
                so.HasCX = station != null;
                so.CXNaturalId = station != null ? station.NaturalId : null;
                so.HasShipyard = planets.Any(p => p.HasShipyard);
                so.ShipyardLocationNaturalIds = planets
                    .Where(p => p.HasShipyard)
                    .Select(p => p.PlanetNaturalId)
                    .ToList();
                so.HasLocalMarket = planets.Any(p => p.HasLocalMarket);
                so.LocalMarketLocationNaturalIds = planets
                    .Where(p => p.HasLocalMarket)
                    .Select(p => p.PlanetNaturalId)
                    .ToList();

                var systemResources = planets
                    .SelectMany(p => p.Resources)
                    .Select(r => new SystemMaterial
                    {
                        MaterialTicker = r.Material,
                        Concentration = r.Concentration,
                        DailyExtraction = r.DailyExtraction
                    });

                so.Planets = planets
                    .Select(p => new Planet
                    {
                        NaturalId = p.PlanetNaturalId,
                        Name = p.PlanetName,
                        DisplayName = p.DisplayName,
                        IsFertile = p.IsFertile,
                        Fertility = p.Fertility,
                        SurfaceType = p.SurfaceType,
                        Gravity = p.Gravity,
                        Pressure = p.Pressure,
                        Temperature = p.Temperature,
                        Resources = p.Resources
                            .Select(r => new SystemMaterial
                            {
                                MaterialTicker = r.Material,
                                Concentration = r.Concentration,
                                DailyExtraction = r.DailyExtraction
                            })
                            .ToList()
                    })
                    .OrderBy(p => p.NaturalId)
                    .ToList();

                so.SystemMaterials.AddRange(systemResources);
                so.Vertices.Add(systemStar.PositionX);
                so.Vertices.Add(systemStar.PositionY);
                so.Vertices.Add(systemStar.PositionZ);

                var planetVertex = new SubSectorVertex(systemStar.PositionX, systemStar.PositionY, systemStar.PositionZ);

                // Now, we need to generate 8 "icon" locations
                // All of these locations will be 45 degrees from each other and 15 units out, but the order is:
                // N, NE, NW, E, W, SE, SW, S
                var degreesFromNorth = new List<double>()
                {
                    0.0, 45.0, -45.0, 90.0, -90.0, 135.0, -135.0, 180.0
                };

                var vector = new SubSectorVertex(0.0, 15.0, 0.0);
                for (int i = 0; i < 8; ++i)
                {
                    double theta = degreesFromNorth[i] * Math.PI / 180.0;
                    var cs = Math.Cos(theta);
                    var sn = Math.Sin(theta);

                    var newVector = new SubSectorVertex();
                    newVector.X = vector.X * cs - vector.Y * sn;
                    newVector.Y = vector.X * sn + vector.Y * cs;

                    var iconVertex = planetVertex + newVector;
                    so.IconVertices.Add(
                        new List<double>
                        {
                            iconVertex.X, iconVertex.Y, iconVertex.Z
                        }
                    );
                }

                if (so.SystemFaction == null) so.SystemFaction = "None";
                switch (so.SystemFaction)
                {
                    case "AI":
                        so.Color = COLOR_FACTION_AI;
                        break;
                    case "CI":
                        so.Color = COLOR_FACTION_CI;
                        break;
                    case "IC":
                        so.Color = COLOR_FACTION_IC;
                        break;
                    case "NC":
                        so.Color = COLOR_FACTION_NC;
                        break;
                    default:
                        so.Color = COLOR_FACTION_NEUTRAL;
                        break;
                }
                so.ColorOnHover = COLOR_ON_HOVER;

                so.ColorOnClick = 0x0000FF;
                so.DotNetMethodToInvokeOnHover = "OnHover";
                so.DotNetMethodToInvokeOnClick = "OnClick";
                Systems.Add(key, so);
            }

            return Systems;
        }
    }
}
